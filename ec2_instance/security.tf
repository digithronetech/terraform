resource "aws_security_group" "group" {
    name = "DefaultGroup"
    tags = {
      Name = "DefaultGroup"
    }
}

resource "aws_security_group_rule" "ssh-in" { # ssh in
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "ssh-out" { # ssh out
    type = "egress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "http-in" { # http in
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "http-out" { # http out
    type = "egress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "https-in" { # https in
    type = "ingress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "https-out" { # https out
    type = "egress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}

resource "aws_security_group_rule" "icmp-out" { #ping out
  type = "egress"
  cidr_blocks = ["0.0.0.0/0"]
  protocol = "icmp"
  from_port = 8
  to_port = 0
  security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "icmp-in" { # ping in
  type = "ingress"
  cidr_blocks = ["0.0.0.0/0"]
  protocol = "icmp"
  from_port = 8
  to_port = 0
  security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "docker1-in" { # Needed for Docker
    type = "ingress"
    from_port = 2377
    to_port = 2377
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}

resource "aws_security_group_rule" "docker1-out" { # Needed for Docker
    type = "egress"
    from_port = 2377
    to_port = 2377
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}

resource "aws_security_group_rule" "docker2-out-tcp" { # Needed for Docker
    type = "egress"
    from_port = 7946
    to_port = 7946
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "docker2-out-udp" { # Needed for Docker
    type = "egress"
    from_port = 7946
    to_port = 7946
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}

resource "aws_security_group_rule" "docker2-in-udp" { # Needed for Docker
    type = "ingress"
    from_port = 7946
    to_port = 7946
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "docker2-in-tcp" { # Needed for Docker
    type = "ingress"
    from_port = 7946
    to_port = 7946
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "docker3-in" { # Needed for Docker
    type = "ingress"
    from_port = 4789
    to_port = 4789
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}
resource "aws_security_group_rule" "docker3-out" { # Needed for Docker
    type = "egress"
    from_port = 4789
    to_port = 4789
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
    security_group_id = aws_security_group.group.id
}