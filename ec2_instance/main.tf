resource "aws_key_pair" "ssh-key" {
  key_name   = "ssh-key"
  public_key = file("ec2_instance/keys/id_rsa.pub")
}

resource "aws_instance" "ubuntu" {
    ami = var.ami-ubuntu
    instance_type = var.instance_type
    count = var.inst_count
    associate_public_ip_address = var.ip_address
    key_name      = aws_key_pair.ssh-key.key_name
    security_groups = [ aws_security_group.group.name ]  

    connection {
    type = "ssh"
    user = "ubuntu"
    host = self.public_ip 
    private_key = file("ec2_instance/keys/id_rsa")
    }
    provisioner "file" {
    source      = "script-ubuntu.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo hostname ubuntu",
      "chmod +x /tmp/script.sh",
      "sudo bash /tmp/script.sh",
    ]
  }
    tags = {
        Name = "${var.name}-${count.index+1}-${var.environment}-${var.project}"
        Region = var.region
        Count = var.inst_count
        Timestamp = replace(timestamp(),":","-")
        Project = var.project
        Environment = var.environment
    }
}






output "region" { value = var.region}
output "ami" { value = var.ami-ubuntu}
output "instance_type" { value = aws_instance.ubuntu.*.instance_type}
output "external_ip" { value = aws_instance.ubuntu.*.associate_public_ip_address}
output "public_ip" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_instance.ubuntu.*.public_ip
}

