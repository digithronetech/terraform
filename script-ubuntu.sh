#!/usr/bin/bash

### EC2 setup script
sudo apt update && apt upgrade -y
sudo apt-get install  apt-transport-https  ca-certificates curl  gnupg-agent software-properties-common -y 
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository  "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"
sudo apt-get update 
sudo apt-get install docker-ce docker-ce-cli containerd.io g++ -y
sudo usermod -a -G docker ubuntu




# join docker swarm 
sudo docker swarm join --token SWMTKN-1-4q1j54jnqi1p2tipn6xcivvvxi9ii2tmpuyy9ae3055zkofhqx-eq5fc3s0an8mby0w8xu70kaoi 24.135.102.157:2377
 