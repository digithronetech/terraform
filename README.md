## AWS+Terraform Docker Swarm

---

## Goal:

The goal is to setup a docker swarm, and execute a service accross it

## Description:

This setup is to achieve setting up a Docker swarm, distributed on a pre-defined number of EC2 instances.
It includes a custom VPC ( with firewall rules ) and a custom script used to setup the docker swarm.

###### Additonal:

The code is on bitbucket, and is linked with Jenkins. In the current use case, Jenkins is used to execute the terraform script and set everything up.
Upon completion, the .tfstate files are pushed to the repo for consistency purposes.


Jenkins executes build.sh to initiate the terraform.
script-*.sh is used on each EC2 instance to set it up a sa a Docker swarm node.