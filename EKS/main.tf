resource "aws_eks_cluster" "aws_eks" {
  name     = "eks_cluster"
  role_arn = aws_iam_role.eks_cluster.arn

  vpc_config {
    subnet_ids = for num in aws_subnet.vpc-private.id.ids:
    cidrsubnet(data.aws_vpc.example.cidr_block, 8, num)
  }

  tags = {
    Name = "EKS"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"
  config = {
    bucket = "terraform-state-prod"
    key    = "save/tfstate/terraform.tfstate"
    region = "eu-central-1"
  }
}
data "aws_vpc" "example" {
  id = var.vpc_id
}
resource "aws_placement_group" "worker" {
  name     = "test"
  strategy = "cluster"
}

resource "aws_autoscaling_group" "bar" {
  name                      = "foobar3-terraform-test"
  max_size                  = 5
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 4
  force_delete              = true
  placement_group           = aws_placement_group.worker.id
  #launch_configuration      = aws_launch_configuration.foobar.name
  vpc_zone_identifier       = for num in aws_subnet.vpc-private.id.ids: cidrsubnet(data.aws_vpc.example.cidr_block, 8, num)

  initial_lifecycle_hook {
    name                 = "foobar"
    default_result       = "CONTINUE"
    heartbeat_timeout    = 2000
    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"

    notification_metadata = <<EOF
{
  "foo": "bar"
}
EOF

    notification_target_arn = "arn:aws:sqs:us-east-1:444455556666:queue1*"
    role_arn                = "arn:aws:iam::123456789012:role/S3Access"
  }

  tag {
    key                 = "foo"
    value               = "bar"
    propagate_at_launch = true
  }

  timeouts {
    delete = "15m"
  }

  tag {
    key                 = "lorem"
    value               = "ipsum"
    propagate_at_launch = false
  }
}
