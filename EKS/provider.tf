provider "aws" {
   profile    = "default"
   region     = "eu-west-3"
 }

 terraform {
   backend "s3" {
     bucket = "bucket-jasduhobc918AHmaqi19"
     key    = "save/tfstate"
     region = "eu-central-1"
   }
 }
