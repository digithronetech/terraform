resource "aws_vpc" "vpc"
{
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "main"
  }
}
data "aws_subnet" "vpc"
{
  for_each = data.aws_subnet_ids.vpc.ids
  id       = each.value
}

resource "aws_vpc_dhcp_options" "vpc" #ip assignment
{
  domain_name_servers = ["8.8.8.8", "1.1.1.1"]
  tags =
  {
    Name = "dhcp-options"
  }
}

resource "aws_vpc_dhcp_options_association" "vpc_dns_resolver" #associate options with vpc
{
  vpc_id          = aws_vpc.vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.vpc.id
}

resource "aws_subnet" "vpc-private" #Private subnet
{
  vpc_id     = aws_vpc.vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
  Name = "private"
  }
}

resource "aws_internet_gateway" "vpc" #Internet gateway
{
  vpc_id = aws_vpc.vpc.id

  tags =
  {
    Name = "Gateway"
  }
}

resource "aws_eip" "vpc-net-eip" # Elastic IP
{
  depends_on = [aws_internet_gateway.vpc]
  instance = aws_instance.web.id # what would be considered an instance ?
  vpc      = true
}

resource "aws_route" "vpc-internet_access" # routing table for internet access
{
  route_table_id            = "rtb-4fbb3ac4"
  destination_cidr_block    = "10.0.1.0/22"
  vpc_peering_connection_id = "pcx-45ff3dc1"
  depends_on                = [aws_route_table.vpc-private] # Likely not for vpc-private
}

resource "aws_route_table" "vpc-private" # routing table for the private subnet
{
  vpc_id = aws_vpc.vpc.id
  tags =
  {
    Name = "private-route-table"
  }
}

resource "aws_route_table_association" "vpc-private" # assignment of the routing table for the private subnet ?
{
  subnet_id      = aws_subnet.vpc-private.id
  route_table_id = aws_route_table.vpc-private.id
}

resource "aws_route" "vpc-private" # routing table for the private network/subnet
{
  route_table_id            = "rtb-4fbb3ac5"
  destination_cidr_block    = "10.0.2.0/22"
  vpc_peering_connection_id = "pcx-45ff3dcx"
  depends_on                = [aws_route_table.vpc-private] # Likely IS for vpc-private
}

resource "aws_subnet" "vpc-private" # Private subnet ( again ? )


resource "aws_nat_gateway" "vpc-nat" # NAT gateway, for internet access ?
{
  allocation_id = aws_eip.vpc-net-eip.id
  subnet_id     = aws_subnet.vpc.id
}

resource "aws_route_table_association" "vpc_public_private" # routing table between the public/private networks ?
