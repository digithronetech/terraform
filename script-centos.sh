#!/usr/bin/bash

### EC2 setup script
sudo yum install -y yum-utils

sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce docker-ce-cli containerd.io
sudo systemctl start docker

# join docker swarm 
sudo docker swarm join --token SWMTKN-1-5wwal35gbq6g7ctyp7akcj91o8wwi62cdikv5ml3ttkdkvtcy6-3pwq7d8pk3eu0d6wq67wmyu3h 24.135.102.157:2377