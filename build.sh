#Executed by Jenkins.
#Trigger URL for build: https://jenkins.digithrone.net/job/Terraform/build?token=0cd8bd18254c37f8b
#Build time ~5m for 3 EC2 instances ( Ubuntu )

#Terraform EC2 instances

terraform init -input=false ec2_instance/
terraform plan  -input=false ec2_instance/
terraform apply -auto-approve ec2_instance/

#Terraform VPC

terraform init -input=false vpc/ 
terraform plan  -input=false vpc/
terraform apply -auto-approve vpc/

#push to bitbucket
git add .
git commit -m "Automated commit"
git push git@bitbucket.org:digithronetech/terraform.git HEAD:master


#execute docker service
docker service create --replicas 3 --name helloworld-$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c10) alpine ping docker.com